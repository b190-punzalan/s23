console.log("Pokemon battle");

/*
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.

*/

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		misty:["hoenn", "kanto"],
		hoenn: ["may", "max"],
		kanto: ["brock"]
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu, I choose you!");
	}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();


// Create a constructor for creating a pokemon with the following properties:
	/*
	- Name (Provided as an argument to the contructor)
	- Level (Provided as an argument to the contructor)
	- Health (Create an equation that uses the level property)
	- Attack (Create an equation that uses the level property)
	*/
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		let a = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + a);
		
		if(a <= 0){
			console.log(target.name + " fainted.");	
		}
	};

	this.faint=function(){
		console.log(target.name + "fainted.");
	}
};

let pikachu = new Pokemon ("Pikachu", 12);
let meowth = new Pokemon ("Meowth", 8);
let charmander = new Pokemon ("Charmander", 10);
let jigglypuff = new Pokemon ("Jigglypuff", 20);

console.log(pikachu);
console.log(meowth);
console.log(charmander);
console.log(jigglypuff);

// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
meowth.tackle(charmander);
jigglypuff.tackle(meowth);

console.log(meowth);
// tackle(meowth);
// meowth.tackle();
// meowth.Pokemon();
// Pokemon(meowth.tackle);
// Pokemon(meowth);
// this.tackle(meowth);

