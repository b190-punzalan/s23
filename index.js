console.log("hello world");

// with an object, we can easily give a label to each value. sa array kasi, limited pwede mo malaman, di mo sila ma-label. like sa example ng grades- sa object pwede mo ilabel ng grading period

// Section- Objects
/*
- objects are data types that are used to represent real world objects
-collection of related data and/or functionalities;
-in Javascript, most features like strings and arrays are considered to be objects
- their difference in Javascript objects is that the JS object has "key: value" pair
- keys are also referred to as "properties" while the value is the figure on the right side of the colon
- objects are commonly initilized or declared using the let + objectName and this object value will start with curly brace{} that is also called "Object Literals"
*/

/*Syntax (using initializers):
let/const objectName = {
	keyA: valueA,
	keyB: valueB
}
*/

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}
console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);

// create an object called car 

/*let car = {
	name: "Toyota Innova",
	yearReleased: 2017
}
console.log(car);*/


// creating objects using constructor function
// this is useful for creating multiple instances/copies of an object
//  - instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it

/*Syntax
	function ObjectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB
	}
*/

/*
this keyword allows to assign a new property for the object by associating them with values received from  a constructor function's parameters*/

// instantiate = tinawag yung object
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

// this is a unique instance/copy of the Laptop object

/*
"new" keyword creates an instance of an object. pag wala kasi yung new na keyword, magiging undefined ung values, and wala ka naman return statement
*/

let laptop = new Laptop("Dell", 2012);
console.log("Result from creating objects using initializers");
console.log(laptop);
console.log(typeof laptop);

// cfeates a new Laptop object
let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using initializers");
console.log(laptop2);
console.log(typeof laptop2);

// create empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using initializers");
console.log(myComputer);
console.log(typeof myComputer);

//accessing objects
/*
-in arrays, we can access the elements inside through the use of square notation: arrayName[index]
- in javascript, it's a bit different since we have to use dot notation to access the property/key
*/

/*
Syntax

*/

// using dot notation
console.log("Result from dot notation" + laptop.name);
// this means i-aaccess ko si name property na nasa loob ni laptop object
// kung gagamit ng square bracket notation= laptop[name], return ay undefined, kasi wala namang element inside the array na pwedeng iaccess using the square bracket notation

// array of objects
/*
- accessin arrays would be the priority since that is the first data type that we have to access
- since we use [] to access we will use laptops[1] to access the laptop2 element inside the laptops array
- since the laptop2 is an object, access its properties would require us to use dot notation
-meaning we have to use laptop2.name to get the value for the "name property"
- hence, we have this syntax:
	arrayName[index].property
*/
let laptops = [laptop, laptop2];
// kung gusto natin i-access name ni laptop2, mag-umpisa ka muna mag-access from outside which is the array name, tapos papasok ka sa loob, pang-ilang index ba si laptop2 and finally dot notation:
console.log(laptops[1].name);

// accessing laptops array - laptop - manufacturedDate
console.log(laptops[0].manufacturedDate);

// using string inside the second sqaure bracket would let us access the property inside the object
console.log(laptops[0]["manufacturedDate"]);
// pede din to kasi pareho ang mapiprint pero mas maganda pa rin yung dot notation kasi baka maconfuse na array yung data type since enclosed siya sa square brackets


// Section - initializing, adding, deleting and reassigning object properties
/*
-like any other variables in javascript, objects may have their properties initialized or added after the object was created/declared
- this is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};
console.log(car);
// adding properties
car.name="Honda Vios";
car.manufacturedDate=2022;
console.log(car);

// while using square brackets will give the same feature as using dot notation, it might lead us to create unconventional naming for the properties which might lead to future confusion when we try to access them
// lilitaw kasi sila pareho, may space lang between manufactured and date for 2019 pero ang conventional way ay camelCasing
car["manufactured date"] = 2019;
console.log(car);

//deleting of properties
delete car["manufactured date"];
console.log(car);
// delete car.manufacturedDate; can also be used with dot notation

// reassigning of properties
/*
change the name of the car into "Dodge Charger R/T"
*/
car.name = "Dodge Charger R/T";
console.log(car);

// Object Methods
/*
- an object method is a function that is set by the dev to be the value of one of the properties
- they are also functions and one of the difference they have is that methods are functions related to a specific object
- these are useful for creating object-specific functions which are used to eprfor tasks on them
-similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how they should do it
*/

/*when we try to ousource a function to be stored inside a property, it would return an error since object methods have to be defined inside an object
function personTalk(){
}
*/
let person = {
	name: "John",
	talk:function(){
		console.log("Hello! My name is " + this.name)
	}
}
console.log(person);
console.log("Result from object methods:");
person.talk();


// adding a method
/*
- kung add, equals ang gagamitin instead of colon. kasi ang colon ay for property inside the object kagaya nung talk sa taas
*/
person.walk = function() {
		console.log(this.name + " walked 25 steps");
}
person.walk()

// shorthand for 
/*template literals backticks(``) + ${} + string data type*/
// example: console.log(`${this.name}walked 25 steps`);

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	// nested object
	address: {
		city: "Austin",
		state: "Texas"
	},
	// nested array
	emails: ["joe@mail.com", "johnHandsome@mail.com"],
	introduce:function(){
		console.log("Hello! My name is " + this.firstName + "" + this.lastName)
	}
}
friend.introduce();
console.log(friend);

// real world application of objects
/*
Scenario
-we would like to create a game that would have several pokemon interact with each other
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle:function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},
	faint:function(){
		console.log("Pokemon Fainted");
	}
};
console.log(myPokemon);

// using constructor function
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint=function(){
		console.log(this.name + "fainted.");
	}
}

// create new pokemon
let jigglypuff = new Pokemon ("jigglypuff", 20);
let charmander = new Pokemon ("charmander", 10);

// using the tackle method from JigglyPuff

jigglypuff.tackle(charmander);

